package processTwo;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.camunda.bpm.spring.boot.starter.event.PreUndeployEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.EventListener;


@Slf4j
@SpringBootApplication
@EnableProcessApplication("myProcessApplicationNameTwo")
public class ProcessTwo {


    public static void main(String[] args) {
        SpringApplication.run(ProcessTwo.class, args);

    }


    @EventListener
    public void onPostDeploy(PostDeployEvent event) {
        log.info("postDeploy: {}", event);
    }

    @EventListener
    public void onPreUndeploy(PreUndeployEvent event) {
        log.info("postDeploy: {}", event);
    }

}
