package processTwo.delegates;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface GreetingsStreams {

    String INPUT = "solicitudes-out";

    @Input(INPUT)
    SubscribableChannel inboundGreetings();


}
