package processTwo.delegates;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Slf4j
@Service("validarSolicitud")
public class ValidarSolicitudDelegate implements JavaDelegate {


    public void execute(DelegateExecution execution) throws Exception {

        log.info("Ejecutando validacion y enviando mensaje");


        execution.setVariable("isValidReques", Boolean.TRUE);

    }

}
