package processOne.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import processOne.util.DataSourceUtil;

import javax.sql.DataSource;

@Slf4j
@Configuration
public class DataBaseConfig {

    @Primary
    @Bean(destroyMethod = "close")
    public DataSource dataSource(Environment environment) {
        String driver = environment.getRequiredProperty("spring.datasource.driver-class-name");
        String url = environment.getRequiredProperty("spring.datasource.url");
        String username = environment.getRequiredProperty("spring.datasource.username");
        String password = environment.getRequiredProperty("spring.datasource.password");
        //String schema = getSchema(environment);

        return DataSourceUtil.builder()
                .driver(driver)
                .url(url)
                .username(username)
                .password(password)
                .build();
    }

}
