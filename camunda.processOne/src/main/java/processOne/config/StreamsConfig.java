package processOne.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import processOne.delegates.GreetingsStreams;

@EnableBinding(GreetingsStreams.class)
public class StreamsConfig {
}
