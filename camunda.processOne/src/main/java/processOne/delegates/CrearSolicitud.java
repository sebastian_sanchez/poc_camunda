package processOne.delegates;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("crearSolicitud")
public class CrearSolicitud implements JavaDelegate {

    private final GreetingsService greetingsService;

    @Autowired
    public CrearSolicitud(GreetingsService greetingsService) {
        this.greetingsService = greetingsService;
    }


    public void execute(DelegateExecution execution) throws Exception {

        log.info("Ejecutando creacion y enviando mensaje");
        greetingsService.sendGreeting(Greetings.builder()
                .message("afsdfsdf broo")
                .timestamp(System.currentTimeMillis())
                .build());


    }

}
