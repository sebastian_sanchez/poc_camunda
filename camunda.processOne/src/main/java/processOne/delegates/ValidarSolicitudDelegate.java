package processOne.delegates;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("validarSolicitud")
public class ValidarSolicitudDelegate implements JavaDelegate {

    private final GreetingsService greetingsService;

    @Autowired
    public ValidarSolicitudDelegate(GreetingsService greetingsService) {
        this.greetingsService = greetingsService;
    }


    public void execute(DelegateExecution execution) throws Exception {

        log.info("Ejecutando validacion y enviando mensaje");
        greetingsService.sendGreeting(Greetings.builder()
                .message("Hiiiiii broo")
                .timestamp(System.currentTimeMillis())
                .build());


        execution.setVariable("isValidReques", Boolean.TRUE);

    }

}
