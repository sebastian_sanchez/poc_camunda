package processOne.delegates;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface GreetingsStreams {

    String OUTPUT = "solicitudes-out";


    @Output(OUTPUT)
    MessageChannel outboundGreetings();
}
