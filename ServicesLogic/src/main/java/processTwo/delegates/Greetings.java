package processTwo.delegates;

import lombok.*;

@Data
@NoArgsConstructor
public class Greetings {

    private long timestamp;
    private String message;

    @Builder
    public Greetings target(long timestamp, String message) {
        Greetings greetings = new Greetings();
        greetings.timestamp = timestamp;
        greetings.message = message;
        return greetings;
    }
}
