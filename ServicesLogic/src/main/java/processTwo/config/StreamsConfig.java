package processTwo.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import processTwo.delegates.GreetingsStreams;

@EnableBinding(GreetingsStreams.class)
public class StreamsConfig {
}
