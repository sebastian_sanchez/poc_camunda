package processTwo.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Builder;

import javax.sql.DataSource;

public class DataSourceUtil {

    private static final String DEFAULT_SCHEMA_SQL = "ALTER SESSION SET CURRENT_SCHEMA=";

    private DataSourceUtil() {
    }

    @Builder
    @SuppressWarnings("squid:S00107")
    public static DataSource target(String driver, String url,
                                    String username, String password,
                                    String schema) {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(driver);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setMaximumPoolSize(10);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("useServerPrepStmts", "true");
        config.setLeakDetectionThreshold(1200000); // 20 minutos
        config.setConnectionTimeout(90000); // minuto y medio

        HikariDataSource dataSource = new HikariDataSource(config);


        if (schema != null) {
            dataSource.setConnectionInitSql(DEFAULT_SCHEMA_SQL + schema);
        }

        return dataSource;
    }
}
